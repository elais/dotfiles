;;; ~/.doom.d/config.el -*- lexical-binding: t; -*-

;; load modules
(load! "+modal")
(load! "+lang!emacslisp")
(load! "+lang!javascript")
(load! "+ui")
(load! "+tools!dash")
(load! "+tools!lsp")
(load! "+pretty-fonts")

;; Editor
(setq-default evil-shift-width 2
              tab-width 2
              c-basic-offset 2
              evil-escape-key-sequence "fd")

;; Credentials
(setq user-email-address "elais.jackson2@bbva.com"
      user-full-name "Elais Jackson")

;; Lisp
(setq lisp-indent-offset nil)

;; Miscellaneous Functions
