;;; ~/Documents/dotfiles/doom/+tools!lsp.el -*- lexical-binding: t; -*-

(setq lsp-session-file (concat doom-etc-dir "lsp-session")
      lsp-auto-guess-root t
      lsp-before-save-edits t
      lsp-keep-workspace-alive nil
      lsp-lens-check-interval 1.0
      lsp-eldoc-enable-hover nil
      lsp-ui-doc-max-height 8
      lsp-ui-doc-max-width  35
      lsp-ui-sideline-enable nil)

;; Don't prompt to restart LSP servers while quitting Emacs
(add-hook! 'kill-emacs-hook (setq lsp-restart 'ignore))

(provide '+tools!lsp)
