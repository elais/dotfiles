;;; ~/Documents/dotfiles/doom/+graphql.el -*- lexical-binding: t; -*-

(def-package! polymode
  :config
  (defcustom pm-inner/js-gql-fenced-code
    (pm-inner-auto-chunkmode :name "js-gql-fenced-code"
                             :head-matcher "^[ ](?:gql`)$"
                             :tail-matcher "^(?:`;|,)$"
                             :body 'graphql-mode))
  (define-polymode poly-graphql-mode
    :hostmode 'pm-host/js
    :innermodes 'pm-inner/js-gql-fenced-code))
