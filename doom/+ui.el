;;; ~/.config/doom/+ui.el -*- lexical-binding: t; -*-

;; meta
(setq mac-option-modifier 'nil)

;; no line numbers
(setq display-line-numbers-type nil)

;; rainbow-identifiers
(defvar +ui/rainbow-identifiers-timer nil
  "Timer for running `+ui/rainbow-identifiers-refresh'.")

(defun +ui/rainbow-identifiers-refresh ()
  "Refresh the set of identifiers in the current buffer."
  (interactive)
  (when rainbow-identifiers-mode (+ui/rainbow-identifiers-refontify)))

(defun +ui/rainbow-identifiers-refontify ()
  "Refontify the buffer using font-lock."
  (if (fboundp 'font-lock-flush)
      (font-lock-flush)
    (when font-lock-mode
      (with-no-warnings
        (font-lock-fontify-buffer)))))

(defun +ui/rainbow-identifiers-mode-hook ()
  "Mode hook for them boys."
  (let ((faces '(font-lock-variable-name-face)))
    (dolist (face faces)
      (face-remap-add-relative face '((:foreground ""))))))

(def-package! rainbow-identifiers
  :hook ((js-mode . rainbow-identifiers-mode)
         (emacs-lisp-mode . rainbow-identifiers-mode)
         (clojure-mode . rainbow-identifiers-mode))
  :init
  (setq rainbow-identifiers-choose-face-function 'rainbow-identifiers-cie-l*a*b*-choose-face
        rainbow-identifiers-cie-l*a*b*-lightness 38
        rainbow-identifiers-cie-l*a*b*-saturation 62
        rainbow-identifiers-cie-l*a*b*-color-count 256)
  :config
  (setq +ui/rainbow-identifiers-timer (run-with-idle-timer 1 t '+ui/rainbow-identifiers-refresh)))

;; tao theme
(def-package! tao-theme
  :init
  (setq tao-theme-use-sepia nil
        tao-theme-use-boxes nil)
  (load-theme 'tao-yang t nil))

;; mac stuff
(when (memq window-system '(mac ns))
  (mac-auto-operator-composition-mode)
  (add-to-list 'default-frame-alist '(ns-appearance . light))
  (add-to-list 'default-frame-alist '(ns-transparent-titlebar . t)))

(provide '+ui)
