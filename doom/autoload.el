;;; ~/Documents/dotfiles/doom/+autoload.el -*- lexical-binding: t; -*-

;;-------------------------------------------------------------------------------
;; Javascript
;;-------------------------------------------------------------------------------

;; Commands

;;;###autoload
(defun +lang!javascript/open-repl ()
  "Open a Javascript REPL. Meaning either `indium-repl', if
indium-interaction-mode is enabled, or `nodejs-repl' otherwise."
  (interactive)
  (call-interactively
   (if (and (featurep 'indium) (indium-interaction-mode))
       #'indium-switch-to-repl-buffer
     #'nodejs-repl)))

;;;###autoload
(defun +lang!javascript/attach-indium-to-this-buffer ()
  "Launch `indium-interaction-mode' in this buffer."
  (interactive)
  (require 'indium)
  (unless (indium-interaction-mode)
    (pcase major-mode
      ('js2-mode (indium-interaction-mode +1))
      (_ (error "Invalid mode %s" major-mode)))))

;; Hooks

;;;###autoload
(defun +lang!javascript|add-node-modules-path ()
  "Add current project's `node_modules/.bin` to `exec-path', so js tools
prioritize project-local packages over global ones."
  (make-local-variable 'exec-path)
  (cl-pushnew (expand-file-name "node_modules/.bin/"
                                (or (locate-dominating-file
                                     (or (buffer-file-name) default-directory)
                                     "node_modules")
                                    (doom-project-root)))
              exec-path :test #'string=))

;;;###autodef
(defalias 'lsp! #'lsp)
