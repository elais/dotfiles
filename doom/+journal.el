;;; ~/Documents/dotfiles/doom/+journal.el -*- lexical-binding: t; -*-

(setq org-hide-emphasis-markers t)

(add-hook 'org-mode-hook 'turn-on-visual-line-mode)

(org-babel-do-load-languages
 'org-babel-load-languages
 '((clojure . t)
   (sh . t)
   (dot . t)
   (emacs-lisp . t)))

(setq org-babel-clojure-backend 'cider)
