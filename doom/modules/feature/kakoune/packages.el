;; -*- no-byte-compile: t; -*-
;;; feature/kakoune/packages.el

(package! ryo-modal)
(package! expand-region)
(package! multiple-cursors)
