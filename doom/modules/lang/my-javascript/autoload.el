;;; lang/my-javascript/autoload.el -*- lexical-binding: t; -*-

(defvar +my-javascript-npm-conf (make-hash-table :test 'equal))

;;;###autoload
(defun +my-javascript-npm-conf (&optional project-root refresh-p)
  "Retrieves an alist of this project's 'package.json'. If REFRESH-P is non-nil
ignore the cache."
  (let ((project-root (or project-root (doom-project-root))))
    (or (and (not refresh-p)
             (gethash project-root +my-javascript-npm-conf))
        (let ((package-file (expand-file-name "package.json" project-root)))
          (when-let* ((json (and (file-exists-p package-file)
                                 (require 'json)
                                 (json-read-file package-file))))
            (puthash project-root json +my-javascript-npm-conf))))))

;;;###autoload
(defun +my-javascript-npm-dep-p (packages &optional project-root refresh-p)
  (when-let* ((data (and (bound-and-true-p +my-javascript-npm-mode)
                         (+my-javascript-npm-conf project-root refresh-p))))
    (let ((deps (append (cdr (assq 'dependencies data))
                        (cdr (assq 'devDependencies data)))))
      (cond ((listp packages)
             (funcall (if (eq (car packages) 'and)
                          #'cl-every
                        #'cl-some)
                      (lambda (pkg) (assq pkg deps))
                      (if (listp packages) packages (list packages))))
            ((symbolp packages)
             (assq packages deps))
            (t (error "Expected a package symbol or list, got %s" packages))))))
;;
;; Commands
;;;###autoload
(defun +my-javascript/repl ()
  "Open a Javascript REPL."
  (interactive)
  (call-interactively
   (if (featurep 'indium)
       #'indium-switch-to-repl-buffer
     #'nodejs-repl)))

;;;###autoload
(defun +my-javascript/indium-jump-off ()
  (interactive)
  (indium-launch))

;;;###autoload
(defun +my-javascript/indium-jump-in ()
  (interactive)
  (indium-connect))

;;;###autoload
(defun +my-javascript/indium-cleanup ()
  "Disable indium in current buffer"
  (interactive)
  (dolist (buf (buffer-list))
  (with-current-buffer buf
    (if indium-interaction-mode (indium-interaction-mode -1)))))

;;
;; Hooks
;;;###autoload
(defun +my-javascript|add-node-modules-path ()
  "Add current project's `node_modules/.bin` to `exec-path', so js tools
prioritize project-local packages over global ones."
  (make-local-variable 'exec-path)
  (cl-pushnew (expand-file-name "node_modules/.bin/"
                                (or (locate-dominating-file
                                     (or (buffer-file-name) default-directory)
                                     "node_modules")
                                    (doom-project-root)))
              exec-path :test #'string=))

;; Advice
;;;###autoload
(defun +my-javascript*indium-project-root ()
  "Resolve to `doom-project-root' if `indium-project-root' fails."
  (or (locate-dominating-file default-directory ".indium.json")
      (or (doom-project-root)
          default-directory)))
