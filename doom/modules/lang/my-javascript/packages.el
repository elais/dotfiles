;; -*- no-byte-compile: t; -*-
;;; lang/my-javascript/packages.el

(package! eslintd-fix)
(package! js2-mode)
(package! js2-refactor)
(package! nodejs-repl)
(package! indium)
(package! npm-mode)
(package! tern)

(when (featurep! :feature completion)
  (package! company-tern))


(when (featurep! :feature lookup)
  (package! xref-js2))
