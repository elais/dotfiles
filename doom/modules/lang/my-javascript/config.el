;;; lang/my-javascript/config.el -*- lexical-binding: t; -*-

;; `js2-mode'
(def-package! js2-mode
  :mode "\\.m?js\\'"
  :interpreter "node"
  :commands js2-line-break
  :config
  (setq js-indent-level 2
        js-chain-indent t
        js2-skip-preprocessor-directives t
        js2-basic-offset 2
        ;; let flycheck handle this
        js2-mode-show-parse-errors nil
        js2-mode-show-strict-warnings nil
        ;; Flycheck provides these features, so disable them: conflicting with
        ;; the eslint settings.
        js2-strict-trailing-comma-warning nil
        js2-strict-missing-semi-warning nil
        ;; maximum fontification
        js2-highlight-level 3
        js2-highlight-external-variables t)

  (add-hook 'js2-mode-hook #'rainbow-delimiters-mode)

  ;; Indent switch-case another step
  (setq-hook! 'js2-mode-hook
    js-switch-indent-offset js2-basic-offset
    mode-name "JS2")
  (set-electric! 'js2-mode :chars '(?\} ?\) ?. ?:))
  (map! :localleader
        :map js2-mode-map
        "'"  'indium-launch
        "\"" 'indium-connect
        "x"  'indium-scratch))

;; `xref-js2'
(def-package! xref-js2
  :when (featurep! :feature lookup)
  :after js2-mode
  :config
  (set-lookup-handlers! 'js2-mode
    :xref-backend #'xref-js2-xref-backend))

;; `js2-refactor'
(def-package! js2-refactor
  :hook (js2-mode . js2-refactor-mode)
  :config
  (when (featurep! :feature evil +everywhere)
    (let ((js2-refactor-mode-map (evil-get-auxiliary-keymap js2-refactor-mode-map 'normal t t)))
      (js2r-add-keybindings-with-prefix (format "%s r" doom-localleader-key)))))

;; `eslintd-fix'
(def-package! eslintd-fix
  :commands eslintd-fix
  :config
  (defun +my-javascript|set-flycheck-executable-to-eslint ()
    (setq flycheck-javascript-eslint-executable eslintd-fix-executable))
  (add-hook 'eslintd-fix-mode-hook #'+my-javascript|set-flycheck-executable-to-eslint))

;; `indium'
(def-package! indium
  :after js2-mode
  :delight
  :init
  :hook (js2-mode . indium-interaction-mode)
  :config
  (setq-local doom-scratch-buffer-display-fn 'indium-scratch-setup-buffer)
  (remove-hook 'indium-client-connected-hook 'indium-repl-setup)
  (set-eval-handler! 'js2-mode #'indium-eval-region)
  (set-repl-handler! 'js2-mode #'indium-switch-to-repl-buffer)
  (map! :localleader
        (:map indium-interaction-mode-map
          (:prefix "e"
            "e" #'indium-eval-last-node
            "r" #'indium-eval-region
            "d" #'indium-eval-defun)
          ;; breakpoints
          (:prefix "b"
            "t" #'indium-toggle-breakpoint
            "b" #'indium-add-breakpoint
            "c" #'indium-add-conditional-breakpoint
            "e" #'indium-edit-breakpoint-condition
            "k" #'indium-remove-breakpoint
            "K" #'indium-remove-all-breakpoints-from-buffer
            "a" #'indium-activate-breakpoints
            "d" #'indium-deactivate-breakpoints
            "l" #'indium-list-breakpoints)
          ;; REPL
          (:prefix "s"
            "'"  #'indium-launch
            "\"" #'indium-connect
            "b"  #'indium-switch-to-repl-buffer
            "S"  #'indium-scratch
            "i"  #'indium-repl-inspect
            "c"  #'indium-repl-clear-output
            "p"  #'indium-repl-previous-input
            "n"  #'indium-repl-next-input
            "P"  #'indium-repl-pop-buffer))))

;; `npm-mode'
(map! :after npm-mode
      :localleader
      :map npm-mode-keymap
      :prefix "n"
      "n" #'npm-mode-npm-init
      "i" #'npm-mode-npm-install
      "s" #'npm-mode-npm-install-save
      "d" #'npm-mode-npm-install-save-dev
      "u" #'npm-mode-npm-uninstall
      "l" #'npm-mode-npm-list
      "r" #'npm-mode-npm-run
      "v" #'npm-mode-visit-project-file)


;; `tern'
(def-package! tern
  :hook (js2-mode . tern-mode)
  :config
  (set-lookup-handlers! 'js2-mode
    :definition    #'tern-find-definition
    :documentation #'tern-get-docs)
  (advice-add #'tern-project-dir :override #'doom-project-root))

(def-project-mode! +my-javascript-npm-mode
  :modes (html-mode css-mode web-mode typescript-mode js2-mode rjsx-mode json-mode markdown-mode)
  :when (locate-dominating-file default-directory "package.json")
  :add-hooks (+my-javascript|add-node-modules-path npm-mode))
