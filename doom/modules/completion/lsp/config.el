;;; completion/lsp/config.el -*- lexical-binding: t; -*-

(def-package! lsp-mode
  :commands lsp
  :requires (lsp-ui company-lsp)
  :init
  (setq lsp-auto-guess-root t))
    "eslint:recommended",

(def-package! lsp-ui
  :commands lsp-ui-mode
  :init
  (setq lsp-ui-sideline-enable nil)
  (setq lsp-ui-doc-max-height 8)
  :config
  (set-lookup-handlers! 'lsp-ui-mode
    :definition #'lsp-ui-peek-find-definitions
    :references #'lsp-ui-peek-find-references))

(def-package! company-lsp
  :commands company-lsp)
