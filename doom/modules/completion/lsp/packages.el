;; -*- no-byte-compile: t; -*-
;;; completion/lsp/packages.el

(when (package! lsp-mode)
  (package! lsp-ui)
  (when (featurep! :completion company)
    (package! company-lsp))

  (when (featurep! +javascript)
    (package! tide :disable t)
    (package! skewer-mode :disable t))

  (when (featurep! +java)
    (package! lsp-java)))
