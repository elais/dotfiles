;;; ~/Documents/dotfiles/doom/+lang!javascript.el -*- lexical-binding: t; -*-
(require 'mode-local)
(after! rainbow-identifiers
  (add-hook 'js2-mode-hook 'rainbow-identifiers-mode))
(after! js2-mode
  (setq js-indent-level 2
        js-chain-indent t)
  (setq-mode-local js2-mode rainbow-identifiers-faces-to-override '(font-lock-variable-name-face js2-external-variable js2-function-param)))

  ;; (setq-mode-local js2-mode rainbow-identifiers-faces-to-override '(font-lock-variable-name-face js2-external-variable js2-function-param)))
;; (after! (:any js2-mode web-mode)
;;   (set-docsets! '(js2-mode) "Javascript" "NodeJS")

;;   (set-pretty-symbols! '(js2-mode web-mode)
;;     ;; Functional
;;     :def "function"
;;     :lambda "() =>"
;;     :composition "compose"
;;     ;; Types
;;     :null "null"
;;     :true "true" :false "false"
;;     ;; Flow
;;     :not "!"
;;     :and "&&" :or "||"
;;     :for "for"
;;     :return "return"
;;     ;; Other
;;     :yield "import"))

;; (require 'mode-local)
;; ;; Major Modes
;; (def-package! js2-mode
;;   :mode "\\.m?js\\'"
;;   :hook (js-mode . rainbow-identifiers-mode)
;;   :interpreter "node"
;;   :commands js2-line-break
;;   :init
;;   (setq js-chain-indent t
;;         js-indent-level 2)
;;   :config
;;   (setq js2-skip-preprocessor-directives t
;;         js2-mode-show-parse-errors nil
;;         js2-mode-show-strict-warnings nil
;;         js2-strict-trailing-comma-warning nil
;;         js2-strict-missing-semi-warning nil
;;         js2-highlight-level 3
;;         js2-highlight-external-variables t)

;;   (add-hook 'js2-mode-hook #'rainbow-delimiters-mode)
;;   ;; Indent switch-case another step
;;   (setq-hook! 'js2-mode-hook
;;     js-switch-indent-offset js2-basic-offset
;;     mode-name "JS2")

;;   (set-electric! 'js2-mode :chars '(?\} ?\) ?. ?:))
;;   (set-repl-handler! 'js2-mode #'+lang!javascript/open-repl)

;;   (after! projectile
;;     (add-to-list 'projectile-globally-ignored-directories "node_modules"))
;;   (map! :map js2-mode-map
;;         "→"  (kbd "C-q = C-q >")
;;         "∧"  (kbd "C-q & C-q &")
;;         "∨"  (kbd "C-q | C-q |")
;;         "≠"  (kbd "C-q ! C-q = C-q =")
;;         "≤"  (kbd "C-q < C-q =")
;;         "≥"  (kbd "C-q > C-q =")
;;         "≡"  (kbd "C-q = C-q = C-q =")
;;         "⊣"  (kbd "C-q < C-q <")
;;         "⊢"  (kbd "C-q > C-q > C-q >")
;;         :localleader
;;         "'"  #'indium-launch
;;         "\"" #'indium-connect
;;         "x"  #'indium-scratch
;;         "S"  #'+lang!javascript/attach-indium-to-this-buffer))

;; (def-package! xref-js2
;;   :when (featurep! :feature lookup)
;;   :after js2-mode
;;   :config
;;   (set-lookup-handlers! 'js2-mode
;;     :xref-backend #'xref-js2-xref-backend))

;; (def-package! js2-refactor
;;   :hook (js2-mode . js2-refactor-mode)
;;   :config
;;   (when (featurep! :feature evil +everywhere)
;;     (let ((js2-refactor-mode-map (evil-get-auxiliary-keymap js2-refactor-mode-map 'normal t t)))
;;       (js2r-add-keybindings-with-prefix (format "%s r" doom-localleader-key)))))

;; (def-package! eslintd-fix
;;   :commands eslintd-fix
;;   :config
;;   (defun +lang!javascript|set-flycheck-executable-to-eslint ()
;;     (setq flycheck-javascript-eslint-executable eslintd-fix-executable))
;;   (add-hook 'eslintd-fix-mode #'+lang!javascript|set-flycheck-executable-to-eslint))

;; (map! :localleader
;;       :prefix ("b" . "breakpoints")
;;       (:after indium
;;         :map indium-interaction-mode-map
;;         "t" #'indium-toggle-breakpoint
;;         "b" #'indium-add-breakpoint
;;         "c" #'indium-add-conditional-breakpoint
;;         "e" #'indium-edit-breakpoint-condition
;;         "k" #'indium-remove-breakpoint
;;         "K" #'indium-remove-all-breakpoints-from-buffer
;;         "a" #'indium-activate-breakpoints
;;         "d" #'indium-deactivate-breakpoints
;;         "l" #'indium-list-breakpoints)
;;       :prefix ("e" . "eval")
;;       (:after indium
;;         :map indium-interaction-mode-map
;;         "e" #'indium-eval-last-node
;;         "E" #'indium-eval-defun)
;;       :prefix ("i" . "inspect")
;;       (:after indium
;;         :map indium-interaction-mode-map
;;         "i" #'indium-inspect-expression
;;         ":" #'indium-inspect-expression))

;; ;; `npm-mode'
;; (map! :after npm-mode
;;       :localleader
;;       :map npm-mode-keymap
;;       :prefix "n"
;;       "n" #'npm-mode-npm-init
;;       "i" #'npm-mode-npm-install
;;       "s" #'npm-mode-npm-install-save
;;       "d" #'npm-mode-npm-install-save-dev
;;       "u" #'npm-mode-npm-uninstall
;;       "l" #'npm-mode-npm-list
;;       "r" #'npm-mode-npm-run
;;       "v" #'npm-mode-visit-project-file)
;; ;; tools
;; (add-hook! (js2-mode) #'lsp!)
;; ;; Projects

;; (def-project-mode! +lang!javascript-npm-mode
;;   :modes (html-mode css-mode web-mode js2-mode json-mode markdown-mode)
;;   :when (locate-dominating-file default-directory "package.json")
;;   :add-hooks (+lang!javascript|add-node-modules-path npm-mode))

(provide '+lang!javascript)
