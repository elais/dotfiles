;;; ~/Documents/dotfiles/doom/+lsp-java.el -*- lexical-binding: t; -*-

(def-package! lsp-mode
  :commands lsp)

(def-package! company-lsp
  :commands company-lsp)

(def-package! lsp-java
  :init
  (setq lsp-format-enabled t
        lsp-java-import-maven-enabled t
        lsp-java-format-settings-url "https://raw.githubusercontent.com/google/styleguide/gh-pages/eclipse-java-google-style.xml"
        lsp-java-format-settings-profile "GoogleStyle"
        lsp-java-completion-guess-arguments t
        lsp-save-action-organize-imports t)
  (add-hook 'java-mode-hook #'lsp)
  :config
  (map! (:localleader
          (:map java-mode-map
            "=" #'lsp-format-buffer
            "d" #'lsp-describe-thing-at-point
            ;; refactor
            (:prefix "r"
              "C" #'lsp-java-extract-to-constant
              "M" #'lsp-java-extract-method
              "A" #'lsp-java-add-unimplemented-methods
              "f" #'lsp-java-create-field
              "l" #'lsp-java-create-local
              "p" #'lsp-java-create-parameter
              "i" #'lsp-java-add-import
              "o" #'lsp-java-organize-imports
              "r" #'lsp-rename)
            ;; goto
            (:prefix "g"
              "d" #'lsp-goto-type-definition
              "i" #'lsp-goto-implementation)
            ;; workspace
            (:prefix "w"
              "a" #'lsp-workspace-folders-add
              "r" #'lsp-workspace-folders-remove
              "s" #'lsp-workspace-folders-switch
              "R" #'lsp-restart-workspace)
            ;; project
            (:prefix "p"
              "c" #'lsp-java-classpath-browse
              "p" #'lsp-update-project-configuration
              "u" #'lsp-java-update-user-settings
              "s" #'lsp-symbol-highlight)))))

(def-package! dap-mode
  :after lsp-mode
  :config
  (dap-mode t)
  (dap-ui-mode t))

(def-package! dap-java :after (lsp-java))
(def-package! lsp-java-treemacs :after (treemacs))
