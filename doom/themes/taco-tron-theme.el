;;; ~/.config/doom/themes/taco-tron-theme.el -*- lexical-binding: t; -*-

(deftheme taco-tron "Taco's Tron Theme")

(defgroup taco-tron nil
  "Taco Tron theme customications.
  The theme has to be reloaded after changing anything here.")

;;; Color Constants
(let ((class '((class color) (min-colors 89)))
   (base0   "#010304")
   (base1   "#030508")
   (base2   "#050a0f")
   (base3   "#060d13")
   (base4   "#0b1722")
   (base5   "#132639")
   (base6   "#1f3d5c")
   (base7   "#306191")
   (base8   "#6e9ecf")
   (base9   "#a3c2e0")
   (baseA   "#c6d9ec")
   (baseB   "#dde8f4")
   (baseC   "#ecf2f9")
   (baseD   "#f0f5fa")
   (baseE   "#f7fafc")
   (baseF   "#fbfcfe"))

   (custom-theme-set-faces
    'taco-tron
    ;; +--- Base ---+
    `(bold ((,class (:weight bold))))
    `(bold-italic ((,class (:weight bold :slant italic))))
    `(default ((,class (:foreground ,baseA :background ,base3))))
    `(error ((,class (:foreground ,baseB :weight bold))))
    `(escape-glyph ((,class (:foreground ,baseC))))
    `(font-lock-builtin-face ((,class (:foreground ,baseD :italic t))))
    `(font-lock-comment-face ((,class (:foreground ,baseA))))
    `(font-lock-comment-delimiter-face ((,class (:foreground ,base9 :italic t))))
    `(font-lock-constant-face ((,class (:foreground ,base9 :weight bold))))
    `(font-lock-doc-face ((,class (:foreground ,baseA))))
    `(font-lock-function-name-face ((,class (:foreground ,base9))))
    `(font-lock-keyword-face ((,class (:foreground ,base7 :weight bold))))
    `(font-lock-negation-char-face ((,class (:foreground ,baseE))))
    `(font-lock-preprocessor-face ((,class (:foreground ,baseE :weight bold))))
    `(font-lock-regexp-grouping-backslash ((,class (:foreground ,baseD :weight bold))))
    `(font-lock-regexp-grouping-construct ((,class (:foreground ,base9 :weight bold))))
    `(font-lock-string-face ((,class (:foreground ,base9 :italic nil))))
    `(font-lock-type-face ((,class (:foreground ,base9 :italic t :bold t))))
    `(font-lock-variable-name-face ((,class (:foreground ,baseB))))
    `(font-lock-warning-face ((,class (:inherit warning))))
    `(italic ((,class (:slant italic))))
    `(shadow ((,class (:foreground ,base3))))
    `(underline ((,class (:underline t))))
    `(warning ((,class (:foreground ,baseA :weight bold))))))

;;;###autoload
(when (and (boundp 'custom-theme-load-path) load-file-name)
  (add-to-list 'custom-theme-load-path
               (file-name-as-directory (file-name-directory load-file-name))))

(provide-theme 'taco-tron)
