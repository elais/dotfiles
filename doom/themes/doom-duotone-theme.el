;;; ~/Documents/dotfiles/doom/themes/doom-duotone-theme.el -*- lexical-binding: t; -*-

(require 'color)

(deftheme duotone "A two tone theme.")

(defgroup duotone nil
  "Duotone theme customizations."
  :group 'faces)

(let ((class '((class color) (min-colors 89)))
      (uno0      (if (display-graphic-p) "#2a2734" nil))
      (uno1      (if (display-graphic-p) "#363342" nil))
      (uno2      (if (display-graphic-p) "#545167" nil))
      (uno3      (if (display-graphic-p) "#6c6783" nil))
      (uno4      (if (display-graphic-p) "#787391" nil))
      (uno5      (if (display-graphic-p) "#8e8aa3" nil))
      (uno6      (if (display-graphic-p) "#a4a1b5" nil))
      (uno7      (if (display-graphic-p) "#bab8c7" nil))
      (duo0      (if (display-graphic-p) "#6a51e6" nil))
      (duo1      (if (display-graphic-p) "#e09142" nil))
      (duo2      (if (display-graphic-p) "#ffad5c" nil))
      (duo3      (if (display-graphic-p) "#7c756e" nil))
      (duo4      (if (display-graphic-p) "#ffcc99" nil))
      (duo5      (if (display-graphic-p) "#ffb870" nil))
      (duo6      (if (display-graphic-p) "#8a75f5" nil))
      (duo7      (if (display-graphic-p) "#7a63ee" nil))
      )

;;;; +------------+
;;;; + Core Faces +
;;;; +------------+
  (custom-theme-set-faces 'duotone
   ;; +--- Base ---+
   `(bold ((,class (:weight bold))))
   `(bold-italic ((,class (:weight bold :slant italic))))
   `(cursor ((,class (:foreground "#9a86fd" :background ,duo1))))
   `(font-lock-keyword-face ((,class (:foreground ,duo6))))
   `(font-lock-variable-name-face ((,class (:foreground ,duo0))))
   `(italic ((,class (:slant italic))))
   `(default ((,class (:foreground ,uno7 :background ,uno0))))
   `(hl-line ((,class (:background ,uno1))))
   `(region ((,class (:foreground ,uno7 :background ,uno1))))
   `(underline ((,class (:underline t))))
   )
  )

(provide-theme 'duotone)
