;;; ~/.config/doom/themes/doom-tron-theme.el -*- lexical-binding: t; -*-

;;; +theme|doom-tron.el --- Tron theme

(require 'doom-themes)
(require 'color)
(defgroup doom-tron-theme nil
  "Options for doom-themes"
  :group 'doom-themes)

(defcustom doom-tron-brighter-modeline nil
  "If non-nil, more vivid colors will be used to style the mode-line."
  :group 'doom-tron-theme
  :type 'boolean)

(defcustom doom-tron-brighter-comments nil
  "If non-nil, comments will be highlighted in more vivid colors."
  :group 'doom-tron-theme
  :type 'boolean)

(defcustom doom-tron-comment-bg doom-tron-brighter-comments
  "If non-nil, comments will have a subtle, darker background. Ehancing thier legibility."
  :group 'doom-tron-theme
  :type 'boolean)

(defcustom doom-tron-padded-modeline doom-themes-padded-modeline
  "If non-nil, adds a 4px padding to the mode-line. Can be an integer to
determine the exact padding."
  :group 'doom-tron-theme
  :type '(or integer boolean))

(defcustom doom-tron-region-highlight t
  "Determines the selection highlight style. Can be 'frost, 'snowstorm or t
(default)."
  :group 'doom-tron-theme
  :type 'symbol)

(defun xah-css-convert-color-hex-to-vec (@rrggbb)
  "Convert color *rrggbb from “\"rrggbb\"” string to a elisp vector [r g b], where the values are from 0 to 1.
   Example:
    (xah-css-convert-color-hex-to-vec \"00ffcc\") ⇒ [0.0 1.0 0.8]

   Note: The input string must NOT start with “#”.
   URL `http://ergoemacs.org/emacs/emacs_CSS_colors.html'
   Version 2016-07-19"
  (vector
   (xah-css-normalize-number-scale (string-to-number (substring @rrggbb 0 2) 16) 255)
   (xah-css-normalize-number-scale (string-to-number (substring @rrggbb 2 4) 16) 255)
   (xah-css-normalize-number-scale (string-to-number (substring @rrggbb 4) 16) 255)))

(defun xah-css-normalize-number-scale (@val @range-max)
  "Scale *val from range [0, *range-max] to [0, 1]
   The arguments can be int or float. Return value is float.
   URL `http://ergoemacs.org/emacs/emacs_CSS_colors.html'
   Version 2016-07-19"
  (/ (float @val) (float @range-max)))

(defun xah-css-hex-to-hsl-color (@hex-str)
  "Convert *hex-str color to CSS HSL format. Return a string. Example:  \"ffefd5\" ⇒ \"hsl(37,100%,91%)\"
   Note: The input string must NOT start with “#”.
   URL `http://ergoemacs.org/emacs/emacs_CSS_colors.html'
   Version 2016-07-19"
  (let* (
         ($colorVec (xah-css-convert-color-hex-to-vec @hex-str))
         ($R (elt $colorVec 0))
         ($G (elt $colorVec 1))
         ($B (elt $colorVec 2))
         ($hsl (color-rgb-to-hsl $R $G $B))
         ($H (elt $hsl 0))
         ($S (elt $hsl 1))
         ($L (elt $hsl 2)))
    (format "hsl(%d,%d%%,%d%%)" (* $H 360) (* $S 100) (* $L 100))))
(xah-css-hex-to-hsl-color "6a2387")

(defun doom-tron-golden-scale ()
  "Generate a golden mean based gradient."
  (let ((golden-scale nil)
        (phi (/ (+ 1 (sqrt 5)) 2)))
    (dotimes (n 16)
      (let* ((alpha (round (/ #xFF (expt phi (+ n 1))))))
        (push alpha golden-scale)
        (push (- #xFF alpha) golden-scale)))
    (sort golden-scale '<)))

(defun doom-theme-scale-filter-fn (input-scale)
  "Scale filter function"
  (cl-remove-duplicates
   (cl-remove-if (lambda (it) (or (< it #x05) (> it #xFC)))
                 input-scale)))
(doom-theme-scale-filter-fn (doom-tron-golden-scale))

(def-doom-theme doom-tron
  "A tronesque theme."
  ;; name   default     256       16
  ((base0   '("#010304" "black"   "black"))
   (base1   '("#050a0f" "#191970" "brightmagenta"))
   (base2   '("#0b1722" "#708090" "gray"))
   (base3   '("#1f3d5c" "#9370db" "magenta"))
   (base4   '("#6e9ecf" "#e6e6fa" "white"))
   (base5   '("#c6d9ec" "#cd853f" "red"))
   (base6   '("#ecf2f9" "gray"    "gray"))
   (base7   '("#f7fafc" "#f4a460" "yellow"))
   (base8   '("#fbfcfe" "#9370db" "magenta"))

   (bg             base0)
   (bg-alt         base1)
   (fg             base5)
   (fg-alt         base6)

   (grey      base6)
   (green     '("#2ed285" nil "green"))
   (violet    '("#ff77ff" "#ee83ee" "violet"))
   (orange    '("#ff681f" "#ff4500" "orange"))
   (cyan      '("#13e3db" "#00ffff" "#40e0d0"))
   (dark-cyan base7)
   (blue      base4)
   (dark-blue base7)
   (teal      '("#0097e0" "#4682b4" "#008080"))
   (magenta   '("#ff77ff" nil "magenta"))
   (red       '("#ed1c24" "#ee83ee" "red"))
   (yellow    '("#e0c280" nil "yellow"))

   ;; face categories -- required for all themes
   (highlight      base9)
   (vertical-bar   (doom-darken bg 0.2))
   (selection      base2)
   (builtin        baseB)
   (comments       baseB)
   (doc-comments   baseB)
   (constants      base8)
   (functions      baseD)
   (keywords       base7)
   (methods        base7)
   (operators      base9)
   (type           base8)
   (strings        base8)
   (variables      baseA)
   (numbers        baseC)
   (region         `(,(doom-lighten (car bg-alt) 0.15) ,@(doom-lighten (cdr base8) 0.35)))
   (error          red)
   (warning        yellow)
   (success        baseD)
   (vc-modified    base3)
   (vc-added       baseB)
   (vc-deleted     base8)

   ;; custom categories
   (-modeline-pad
    (when doom-tron-padded-modeline
      (if (integerp doom-tron-padded-modeline)
          doom-tron-padded-modeline 4))))
  ;; --- extra faces ------------------------
  (
  ;; --- major-mode faces -------------------
  ;; js2-mode
   (js2-object-property :foreground fg)
   (js2-object-property-access :foreground fg)
  ;; rainbow-delimiters
   (rainbow-delimiters-depth-1-face   :foreground base7)
   (rainbow-delimiters-depth-2-face   :foreground base8)
   (rainbow-delimiters-depth-3-face   :foreground base9)
   (rainbow-delimiters-depth-4-face   :foreground baseA)
   (rainbow-delimiters-depth-5-face   :foreground baseB)
   (rainbow-delimiters-depth-6-face   :foreground baseC)
   (rainbow-delimiters-depth-7-face   :foreground baseD)
   (rainbow-delimiters-depth-8-face   :foreground baseD)
   (rainbow-delimiters-depth-9-face   :foreground baseD)
   (rainbow-delimiters-depth-10-face  :foreground baseD)
   (rainbow-delimiters-depth-11-face  :foreground baseD)
   (rainbow-delimiters-depth-12-face  :foreground baseD)
   )

  )



(add-to-list 'default-frame-alist '(ns-appearance . dark))

(push '(doom-tron . t) +doom-solaire-themes)

  ;;; +theme|doom-opera.el ends here
