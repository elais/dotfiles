;;; ~/Documents/dotfiles/doom/+modal.el -*- lexical-binding: t; -*-

(def-package! kakoune
  :bind ("C-z" . ryo-modal-mode)
  :config
  (require 'kakoune)
  (add-hook 'prog-mode-hook #'ryo-enter))

(provide '+modal)
