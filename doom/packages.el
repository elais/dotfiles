;; -*- no-byte-compile: t; -*-
;;; ~/.doom.d/packages.el

;;; Examples:
;; (package! some-package)
;; (package! another-package :recipe (:fetcher github :repo "username/repo"))
;; (package! builtin-package :disable t)
(package! tao-theme :recipe (:fetcher github :repo "elais/tao-theme-emacs"))

;; minor modes
(package! highlight-defined)
(package! rainbow-identifiers :recipe (:fetcher github :repo "elais/rainbow-identifiers"))
(package! solaire-mode)

;; modelines
(package! taoline :recipe (:fetcher github :repo "11111000000/taoline"))
(package! feebleline)

;; tools
(package! evil-tutor)
(package! dash)
(package! eslintd-fix)
(package! indium)
(package! kakoune :recipe (:fetcher github :repo "jmorag/kakoune.el"))
