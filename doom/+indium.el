;;; ~/.config/doom/+indium.el -*- lexical-binding: t; -*-

;; funcs
(defun +indium/indium-eval-last-node-and-focus (arg)
  (interactive "P")
  (indium-eval-last-node arg)
  (indium-switch-to-repl-buffer)
  (evil-insert-state))

(defun +indium/indium-eval-region-and-focus (beg end)
  (interactive "r")
  (indium-eval-region beg end)
  (indium-switch-to-repl-buffer)
  (evil-insert-state))

(defun +indium/indium-eval-defun-and-focus ()
  (interactive)
  (indium-eval-defun)
  (indium-switch-to-repl-buffer)
  (evil-insert-state))

(defun +indium/indium-eval-buffer-and-focus ()
  (interactive)
  (indium-eval-buffer)
  (indium-switch-to-repl-buffer)
  (evil-insert-state))

(after! js2-mode
  (def-package! indium
    :hook (js-mode . indium-interaction-mode)
    :init
    (set-repl-handler! 'js-mode #'indium-repl-setup)
    (set-eval-handler! 'js-mode #'indium-eval-region)
    :config
    (map! (:localleader
            (:map js-mode-map
              "'"   #'indium-launch
              "\""  #'indium-connect)
            (:map indium-interaction-mode-map
              ;; eval
              (:prefix "e"
                "e" #'indium-eval-last-node
                "E" #'+indium/indium-eval-last-node-and-focus
                "r" #'indium-eval-region
                "R" #'+indium/indium-eval-region-and-focus
                "d" #'indium-eval-defun
                "D" #'+indium/indium-eval-defun-and-focus
                "b" #'indium-eval-buffer
                "B" #'indium-eval-buffer-and-focus)
              ;; inspect
              (:prefix "i"
                "n" #'indium-inspect-last-node
                "e" #'indium-inspect-last-expression)
              ;; REPL
              (:prefix "R"
                "b" #'indium-switch-to-repl-buffer
                "S" #'indium-scratch
                "i" #'indium-repl-inspect
                "c" #'indium-repl-clear-output
                "p" #'indium-repl-previous-input
                "n" #'indium-repl-next-input
                "P" #'indium-repl-pop-buffer))))))
