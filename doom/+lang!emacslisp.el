;;; ~/Documents/dotfiles/doom/+lang!emacslisp.el -*- lexical-binding: t; -*-

(def-package! highlight-defined
  :hook ((emacs-lisp-mode . highlight-defined-mode))
  :config
  (setq-mode-local clojure-mode rainbow-identifiers-faces-to-override '(highlight-defined-variable-name-face))
  (setq-mode-local emacs-lisp-mode rainbow-identifiers-faces-to-override '(highlight-defined-variable-name-face)))

(provide '+lang!emacslisp)
